extern crate rand;
use rand::Rng;



trait Sort {
    const ZERO: Self;

    fn inc(&mut self);
    fn as_usize(self) -> usize;
}

fn count_sort<D, T>(data: &mut D, min: T, max: T)
    where   for <'a> &'a mut D: IntoIterator<Item = &'a mut T>,
            T: Sort + Clone {
    let mut count = vec![0usize; max.clone().as_usize() - min.clone().as_usize() + 1usize];

    for e in data.into_iter() {
        count[e.clone().as_usize()] += 1usize;
    }

    let mut i = T::ZERO;
    let mut iter = data.into_iter();
    for e in count.iter() {
        for _ in 0usize..*e {
            if let Some(item) = iter.next() {
                *item = i.clone();
            }
        }

        i.inc();
    }
}


impl Sort for u16 {
    const ZERO: Self = 0;

    fn inc(&mut self) {
        *self += 1;
    }

    fn as_usize(self) -> usize {
        self as usize
    }
}


fn main() {
    let mut rng = rand::thread_rng();
    let mut data: Vec<_> = (0..1_048_576).map(|_| rng.gen_range(u16::min_value(), u16::max_value() - 1)).collect();

    // println!("data = {:?}", data);
    count_sort(&mut data, u16::min_value(), u16::max_value() - 1);
    // println!("data = {:?}", data);
}